#include <iostream>
#include <stdlib.h>
#include <stddef.h>
#include <sys/time.h>
#include <unistd.h>
#include <mpi.h>

#include "Solver.h"
#include "P_Solver.h"

int main(int argc, char ** argv) {

	int my_rank; // rank of process
	int p; // number of processes
	int source; // rank of sender
	int dest; // rank of receiver
	int tag = 0; // tag for messages
	//char message[100]; // storage for message
	MPI_Status status; // return status for  receive

	double start, stop;

	// Start up MPI
	MPI_Init(&argc, &argv);

	// process rank
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	// number of processes
	MPI_Comm_size(MPI_COMM_WORLD, &p);

	// start time measure
	MPI_Barrier(MPI_COMM_WORLD);
	start = MPI_Wtime();

	long double dt = strtold(argv[1], NULL);
	long int maxStep = strtold(argv[2], NULL);
	long double tolerance = strtold(argv[3], NULL);
	long int ouputPeriod = strtold(argv[4], NULL);
	long int savePeriod = strtold(argv[5], NULL);

	//Mesh
	Mesh m(x_min,x_max,y_min,y_max,z_min,z_max,ii,jj,kk);

	if (my_rank == 0) {

		// Scalar fields
		ScalarField M(m); // Material distribution in cell
		ScalarField U(m); // Electrical potential
		ScalarField T(m); // Temperature

		// Simulation
		BuildCell(M, true); // Set nodes' material
		T.init(T0); // Set initial temperature of nodes

		ostringstream vtkfilename;
		vtkfilename.str("");
		vtkfilename.clear();
		vtkfilename << "MaterialDistr.vtk";
		M.saveVtk(vtkfilename);

		vtkfilename.str("");
		vtkfilename.clear();
		vtkfilename << "InitialU.vtk";
		U.loadVtk(vtkfilename, 14);

		SolveCoupledProblemOnMaster(U, T, M, dt, maxStep, tolerance, ouputPeriod, savePeriod);
	} else {
		SolveCoupledProblemOnWorkers(dt, m.getDz(), maxStep);
	}

	MPI_Barrier(MPI_COMM_WORLD);  // Wait for completion of all processes
	stop = MPI_Wtime();

	long double elapsed = stop - start;
	if (my_rank == 0) {
		cout << "Elapsed time: " << elapsed << endl;
	}

	MPI_Finalize();

	return 0;
}
