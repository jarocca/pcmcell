#ifndef PARAMS
#define PARAMS

#include <math.h>

// Here you will find the variables and parameters of the simulation

namespace Params
{
	// Numerical parameters
	const int ii = 100;
	const int jj = 100;
	const int kk = 100;

	// Mesh and geometry parameters
	const long double x_min = 0.0;
	const long double x_max = 1.0e-6;
	const long double y_min = 0.0;
	const long double y_max = 1.0e-6;
	const long double z_min = 0.0;
	const long double z_max = 1.0e-6;
	//in m

  // Material dimensions in cell [nodes]
  //Heights
  const int BEC_h = 20; // Botton Electrode Contact height
  const int BE_h = 20; // Botton Electrode height
  const int TE_h = 20; // Top Electrode height
  const int TEC_h = 20; // Top Electrode Contact height
  //Widths
  const int BD_w = 40; // Bottom Dielectric width
  const int TD_w = 30; // Top Dielectric width

  //Material definitions
  const char Dielectric = 0;
  const char ElectrodeContact = 1;
  const char Electrode = 2;
  const char AmorphousChalcogenide = 3;
	const char CrystallineChalcogenide = 4;

	//Electrical parameters
	//Applied electrical potencial difference
	const long double U0 = 5.0;
	//Electrical conductivity in (ohm m)^-1
	const long double DielectricSigma = 1.0e-14;
	//ElectrodeContactSigma is assumed infinite for Laplace Eq: Electrode Contact has the same
	//electrical potential in its whole volume
	//A large value is used for Joule heating calculation
	const long double ElectrodeContactSigma = 18.94e6;
	const long double ElectrodeSigma = 1.0e6;

	long double electricalConductivity(char material, long double temperature) {
		long double ec;
		switch (material) {
			case Dielectric:
				ec = DielectricSigma;
				break;
			case ElectrodeContact:
				ec = ElectrodeContactSigma;
			case Electrode:
				ec = ElectrodeSigma;
				break;
			case AmorphousChalcogenide:
				ec = pow(10.0, -0.53686 + 0.01131*(temperature-273.15)); //Amorphous
				break;
			case CrystallineChalcogenide:
				ec = pow(10.0, 3.51408 + 0.00055353*(temperature-273.15)); //Crystalline
				break;
		}
		return ec;
	}

	//Thermal parameters
	const long double T0 = 298.15;	// Room temperature
	//K: Thermal conductivity in W/(m K)
	const long double DielectricK = 1.4;
  const long double ElectrodeContactK = 175.0;
  const long double ElectrodeK = 13.0;
  const long double AmorphousChalcogenideK = 0.2; //Amorphous
	const long double CrystallineChalcogenideK = 0.5; //Crystalline
	//Cp: Isobaric volumetric heat capacity in J/(m^3 K)
	const long double DielectricCp = 3.1e6;
	const long double ElectrodeContactCp = 2.58e6;
	const long double ElectrodeCp = 3.235e6;
	const long double AmorphousChalcogenideCp = 1.3e6;
	const long double CrystallineChalcogenideCp = 1.3e6;

	long double thermalConductivity(char material) {
		long double tc;
		switch (material) {
			case Dielectric:
				tc = DielectricK;
				break;
			case ElectrodeContact:
				tc = ElectrodeContactK;
				break;
			case Electrode:
				tc = ElectrodeK;
				break;
			case AmorphousChalcogenide:
				tc = AmorphousChalcogenideK;
				break;
			case CrystallineChalcogenide:
				tc = CrystallineChalcogenideK;
				break;
		}
		return tc;
	}

	long double specificHeat(char material) {
		long double sh;
		switch (material) {
			case Dielectric:
				sh = DielectricCp;
				break;
			case ElectrodeContact:
				sh = ElectrodeContactCp;
				break;
			case Electrode:
				sh = ElectrodeCp;
				break;
			case AmorphousChalcogenide:
				sh = AmorphousChalcogenideCp;
				break;
			case CrystallineChalcogenide:
				sh = CrystallineChalcogenideCp;
				break;
		}
		return sh;
	}
}

#endif
