#include <iostream>
#include <stdlib.h>
#include <stddef.h>
#include <sys/time.h>
#include <unistd.h>

#include "Solver.h"
#include <omp.h>

int main(int argc, char ** argv) {
	//Mesh
	Mesh m(x_min,x_max,y_min,y_max,z_min,z_max,ii,jj,kk);
	// Scalar fields
	ScalarField M(m); // Material distribution in cell
	ScalarField U(m); // Electrical potential
	ScalarField T(m); // Temperature

	// Simulation
	BuildCell(M, true); // Set nodes' material
	T.init(T0); // Set initial temperature of nodes

	ostringstream vtkfilename;
	vtkfilename.str("");
	vtkfilename.clear();
	vtkfilename << "MaterialDistr.vtk";
	M.saveVtk(vtkfilename);

	vtkfilename.str("");
	vtkfilename.clear();
	vtkfilename << "InitialU.vtk";
	U.loadVtk(vtkfilename, 14);

	long double t1,t2,elapsed;
  struct timeval tp;
  int rtn;

  rtn=gettimeofday(&tp, NULL);
  t1=(double)tp.tv_sec+(1.e-6)*tp.tv_usec;

	SolveCoupledProblem(U, T, M, strtold(argv[1], NULL), strtold(argv[2], NULL), strtold(argv[3], NULL), strtold(argv[4], NULL), strtold(argv[5], NULL));
	//args: dt, maxStep, tolerance, ouputPeriod, savePeriod

	rtn=gettimeofday(&tp, NULL);
  t2=(double)tp.tv_sec+(1.e-6)*tp.tv_usec;
  elapsed=t2-t1;

  cout << "Elapsed time: " << elapsed << endl;

	return 0;
}
