#ifndef SCALAR_FIELD
#define SCALAR_FIELD

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>

#include "Mesh.h"

using namespace std;

class ScalarField
{

	private:

		int ii;
		int jj;
		int kk;
		int n;
		long double * p_mat;
		const Mesh * mesh; // The mesh can be shared with other scalar field objects

	public:

		ScalarField(const Mesh &mesh0)
		{
			mesh = &mesh0;
			ii = mesh->getII();
			jj = mesh->getJJ();
			kk = mesh->getKK();
			n = ii * jj * kk;
			p_mat = new long double[n];
			init(0);
		}

		void init(long double val)
		{
			for (int l = 0; l < n; l++)
				p_mat[l] = val;
		}

		void set(const int i, const int j, const int k, long double val)
		{
			p_mat[i + j * ii + ii * jj * k] = val;
		}

		long double get(const int i, const int j, const int k) const
		{
			return p_mat[i + j * ii + ii * jj * k];

		}

		const long double* getMatrix() const
		{
			return p_mat;
		}

		const Mesh& getMesh() const
		{
			return *mesh;
		}

		void print() const
		{
			for (int i = 0; i < ii; i++)
			{
				for (int j = 0; j < jj; j++)
				{
					for (int k = 0; k < kk; k++)
					{
						cout << mesh->getX(i)	<< "," << mesh->getY(j) << ","
									<< mesh->getZ(k) << ","
									<< p_mat[i + j * ii + ii * jj * k] << endl;
					}
				}
			}
		}

		void saveCsv(const ostringstream &filename) const
		{
			ofstream myfile1;
			long double aux;
			// Open file
			myfile1.open(filename.str().c_str());

			// Write content
			myfile1 << "X,Y,Z,S" << endl;
			for (int k = 0; k < kk; k++)
			{
				for (int j = 0; j < jj; j++)
				{
					for (int i = 0; i < ii; i++)
					{
						aux = p_mat[i + j * ii + ii * jj * k];
						myfile1 << mesh->getX(i) << "," << mesh->getY(j) << ","
							<< mesh->getZ(k) << "," << aux << endl;
					}
				}
			}
			// Close file name
			myfile1.close();

		}

		// It assums that the file was created with the same dimensions
		void loadVtk(const ostringstream &filename, int headerLines)
		{
			char* end;
			const char* p;
			long double val;
			string line;
			ifstream myfile;
			int l = 0;
			//Open file
			myfile.open(filename.str().c_str());
			if(myfile.is_open())
			{
				for (int hl=0; hl<headerLines; hl++) {
					if (myfile.good())
						getline(myfile, line);
				}
				while(myfile.good())
				{
					getline(myfile, line);
					p = line.c_str();
					val = strtold(p, &end);
					while (p != end)
					{
						if(l < n)
						{
							p_mat[l] = val;
							l++;
						}
						p = end;
						val = strtold(p, &end);
					}
					/* Alternative:
					for (int i = 0; i < ii; i++)
					{
						val = strtold(p, &end);
						if(l < n)
						{
							p_mat[l] = val;
							l++;
						}
						p = end;
					}
					*/
				}

			  myfile.close();
			}
		}

		void saveVtk(const ostringstream &filename) const
		{

			ofstream myfile1;

			// Open file
			myfile1.open(filename.str().c_str());
			// Write content
			myfile1 << "# vtk DataFile Version 2.0\n";
			myfile1 << "Comment goes here\n";
			myfile1 << "ASCII\n";
			myfile1 << "\n";
			myfile1 << "DATASET STRUCTURED_POINTS\n";
			myfile1 << "DIMENSIONS  " << ii << " " << jj << " " << kk << endl;
			myfile1 << "\n";
			myfile1 << "ORIGIN    0.000   0.000   0.000\n";
		  myfile1 << "SPACING    "<< mesh->getDx() <<" "<< mesh->getDy() << " " << mesh->getDz() << "\n";
			myfile1 << "\n";
			myfile1 << "POINT_DATA " << ii * jj * kk << endl;
			myfile1 << "SCALARS scalars float\n";
			myfile1 << "LOOKUP_TABLE default\n";
			myfile1 << "\n";

			for (int k = 0; k < kk; k++)
			{
				for (int j = 0; j < jj; j++)
				{
					for (int i = 0; i < ii; i++)
					{
						myfile1 << p_mat[i + j * ii + ii * jj * k] << " ";
					}
					myfile1 << endl;
				}
			}

			// Close file name
			myfile1.close();
		}

		long double getNorm() const
		{
			long double max = p_mat[0];
			for (int l = 1; l < n; l++)
			{
				if (max < p_mat[l])
					max = p_mat[l];
			}
			return max;
		}

		// It assums that ii, jj and kk doesn't change
		ScalarField& operator =(const ScalarField & other)
		{
			mesh = &other.getMesh();
			ii = mesh->getII();
			jj = mesh->getJJ();
			kk = mesh->getKK();
			if (p_mat == NULL)
			{
				int n = ii * jj * kk;
				p_mat = new long double[n];
			}
			for (int i = 0; i < ii; i++)
			{
				for (int j = 0; j < jj; j++)
				{
					for (int k = 0; k < kk; k++)
					{
						p_mat[i + j * ii + ii * jj * k] = other.get(i, j, k);
					}
				}
			}
			return *this;
		}

		long double & operator()(const int i, const int j, const int k) const
		{
			//return get(i, j, k);
			return p_mat[i+j*ii+ii*jj*k];
		}

		ScalarField operator +(const ScalarField & other) const
		{
			ScalarField * add = new ScalarField(*mesh);
			for (int i = 0; i < ii; i++)
			{
				for (int j = 0; j < jj; j++)
				{
					for (int k = 0; k < kk; k++)
					{
						add->set(i, j, k,
							p_mat[i + j * ii + ii * jj * k]
							- other.get(i, j, k));
					}
				}
			}
			return *add;
		}

		ScalarField operator -(const ScalarField & other) const
		{
			ScalarField * diff = new ScalarField(*mesh);
			for (int i = 0; i < ii; i++)
			{
				for (int j = 0; j < jj; j++)
				{
					for (int k = 0; k < kk; k++)
					{
						diff->set(i, j, k,
							p_mat[i + j * ii + ii * jj * k]
							- other.get(i, j, k));
					}
				}
			}
			return *diff;
		}

		~ScalarField()
		{
			if (p_mat != NULL)
				delete[] p_mat;
			// The mesh can be shared with other scalar field objects
			// ergo I will not delete the Mesh
		}

};

#endif
