#ifndef SOLVER
#define SOLVER

#include <iostream>
#include <iomanip>
#include "ScalarField.h"
#include "Params.h"

using namespace Params;
using namespace std;

void BuildCell(const ScalarField& sf, bool amorphous);
void SetDirichletBoundaryConditions(const ScalarField& sf);
void SolveLaplaceEq_EP(const ScalarField& U, const ScalarField& T, const ScalarField& M, long double tolerance);
void CalculateHeatFlow(const ScalarField& U, const ScalarField& T, const ScalarField& M, const ScalarField& H);
void SolveCoupledProblem(const ScalarField& U, const ScalarField& T, const ScalarField& M, long double dt,
	long int maxStep, long double tolerance, long int outputPeriod, long int savePeriod);

void BuildCell(const ScalarField& sf, bool amorphous) {
	for (int k=0; k<kk; k++) {
		for (int j=0; j<jj; j++) {
			for (int i=0; i<ii; i++) {
				if (k <= BEC_h-1) { // 1st layer: only Bottom Electrode Contact
					sf(i,j,k) = ElectrodeContact;
				} else {
					if ((k >= BEC_h) && (k <= BEC_h + BE_h - 1)) { // 2nd layer: centered Botton Electrode among Dielectric
						if ((i >= BD_w) && (i <= ii - BD_w - 1) && (j >= BD_w) && (j <= jj - BD_w -1)) {
							sf(i,j,k) = Electrode;
						} else {
							sf(i,j,k) = Dielectric;
						}
					} else {
						if ((k >= BEC_h + BE_h) && (k <= kk - (TEC_h + TE_h) - 1)) { // 3rd layer: centered Chalcogenide among Dielectric
							if ((i >= TD_w) && (i <= ii - TD_w - 1) && (j >= TD_w) && (j <= jj - TD_w -1)) {
								if (amorphous) {
										sf(i,j,k) = AmorphousChalcogenide;
								} else {
										sf(i,j,k) = CrystallineChalcogenide;
								}
							} else {
								sf(i,j,k) = Dielectric;
							}
						} else {
							if ((k >= kk - (TEC_h + TE_h)) && (k <= kk - TEC_h - 1)) { //4th layer: centered Top Electrode among Dielectric
								if ((i >= TD_w) && (i <= ii - TD_w - 1) && (j >= TD_w) && (j <= jj - TD_w -1)) {
									sf(i,j,k) = Electrode;
								} else {
									sf(i,j,k) = Dielectric;
								}
							} else {
								if ((k >= kk - TEC_h) && (k <= kk-1)) { // 5th layer: only Top Electrode Contact
									sf(i,j,k) = ElectrodeContact;
								}
							}
						}
					}
				}
			}
		}
	}
}

void SetDirichletBoundaryConditions(const ScalarField& sf) {
	for (int k=0; k<kk; k++) {
		for (int j=0; j<jj; j++) {
			for (int i=0; i<ii; i++) {
				if (k <= BEC_h-1) { // 1st layer: only Bottom Electrode Contact
					sf(i,j,k) = 0;
				} else {
					if ((k >= kk - TEC_h) && (k <= kk-1)) { // 5th layer: only Top Electrode Contact
						sf(i,j,k) = U0;
					}
				}
			}
		}
	}
}

void SolveLaplaceEq_EP(const ScalarField& U, const ScalarField& T, const ScalarField& M, long double tolerance) {
	//long double a, b, c, d, e, f, ijk_ec
	long double diff = 0.0;
	long double dxsqr = pow(U.getMesh().getDx(), 2.0);
	long double dysqr = pow(U.getMesh().getDy(), 2.0);
	long double dzsqr = pow(U.getMesh().getDz(), 2.0);
	int it = 0;
	ScalarField prevU(U.getMesh());
	do {
		prevU = U;
		#pragma omp parallel for schedule(dynamic)
		for (int k=BEC_h; k<kk - TEC_h; k++) { // Skip Top and Botton Electrode Contact layers
			for (int j=1; j<jj-1; j++) { // Skip boundary faces normal to y direction
				for (int i=1; i<ii-1; i++) { // Skip boundary faces normal to x direction
					long double ijk_ec = electricalConductivity(M(i,j,k),T(i,j,k));
					long double a = (electricalConductivity(M(i+1,j,k),T(i+1,j,k)) + ijk_ec)/(2*dxsqr);
					long double b = (electricalConductivity(M(i-1,j,k),T(i-1,j,k)) + ijk_ec)/(2*dxsqr);
					long double c = (electricalConductivity(M(i,j+1,k),T(i,j+1,k)) + ijk_ec)/(2*dysqr);
					long double d = (electricalConductivity(M(i,j-1,k),T(i,j-1,k)) + ijk_ec)/(2*dysqr);
					long double e = (electricalConductivity(M(i,j,k+1),T(i,j,k+1)) + ijk_ec)/(2*dzsqr);
					long double f = (electricalConductivity(M(i,j,k-1),T(i,j,k-1)) + ijk_ec)/(2*dzsqr);
					U(i,j,k) = (a*prevU(i+1,j,k) + b*prevU(i-1,j,k) + c*prevU(i,j+1,k) + d*prevU(i,j-1,k) + e*prevU(i,j,k+1) + f*prevU(i,j,k-1))/(a+b+c+d+e+f);
				}
			}
			// Set Neumann Boundary Conditions: Ux=0, Uy=0
			// Edges
			for (int j=1; j<jj-1; j++) {
				U(0,j,k) = U(1,j,k);
				U(ii-1,j,k) = U(ii-2,j,k);
			}
			for (int i=1; i<ii-1; i++) {
				U(i,0,k) = U(i,1,k);
				U(i,jj-1,k) = U(i,jj-2,k);
			}
			// Corners
			U(0,0,k) = U(1,1,k);
			U(ii-1,0,k) = U(ii-2,1,k);
			U(0,jj-1,k) = U(1,jj-2,k);
			U(ii-1,jj-1,k) = U(ii-2,jj-2,k);
		}
		diff = (U - prevU).getNorm();
		it++;
	} while (diff > tolerance);
	cout << "it: " << it << " diff: " << scientific << diff << endl;
}

void CalculateHeatFlow(const ScalarField& U, const ScalarField& T, const ScalarField& M, const ScalarField& H) {
	//long double a, b, c, d, e, f, ijk_tc;
	long double dx = T.getMesh().getDx();
	long double dy = T.getMesh().getDy();
	long double dz = T.getMesh().getDz();
	long double dxsqr = pow(dx, 2.0);
	long double dysqr = pow(dy, 2.0);
	long double dzsqr = pow(dz, 2.0);

	//long double lapT, Q, Uxsqr, Uysqr, Uzsqr;

	#pragma omp parallel for collapse(3) schedule(dynamic)
	for (int k=1; k<kk-1; k++) {
		for (int j=1; j<jj-1; j++) {
			for (int i=1; i<ii-1; i++) {
				// Heat conduction
				long double ijk_tc = thermalConductivity(M(i,j,k));
				long double a = (thermalConductivity(M(i+1,j,k)) + ijk_tc)/(2*dxsqr);
				long double b = (thermalConductivity(M(i-1,j,k)) + ijk_tc)/(2*dxsqr);
				long double c = (thermalConductivity(M(i,j+1,k)) + ijk_tc)/(2*dysqr);
				long double d = (thermalConductivity(M(i,j-1,k)) + ijk_tc)/(2*dysqr);
				long double e = (thermalConductivity(M(i,j,k+1)) + ijk_tc)/(2*dzsqr);
				long double f = (thermalConductivity(M(i,j,k-1)) + ijk_tc)/(2*dzsqr);

				long double lapT = a*(T(i+1,j,k)-T(i,j,k)) + b*(T(i-1,j,k)-T(i,j,k)) + c*(T(i,j+1,k)-T(i,j,k))
				+ d*(T(i,j-1,k)-T(i,j,k)) + e*(T(i,j,k+1)-T(i,j,k)) + f*(T(i,j,k-1)-T(i,j,k));

				//Joule heating
				long double Uxsqr = pow((U(i+1,j,k) - U(i-1,j,k))/(2*dx), 2.0);
				long double Uysqr = pow((U(i,j+1,k) - U(i,j-1,k))/(2*dy), 2.0);
				long double Uzsqr = pow((U(i,j,k+1) - U(i,j,k-1))/(2*dz), 2.0);
				long double Q = electricalConductivity(M(i,j,k),T(i,j,k))*(Uxsqr + Uysqr + Uzsqr);
				H(i,j,k) = lapT + Q;
			}
		}
	}
}

void SolveCoupledProblem(const ScalarField& U, const ScalarField& T, const ScalarField& M, long double dt,
	long int maxStep, long double tolerance, long int outputPeriod, long int savePeriod) {
	ScalarField H(T.getMesh());

	long double t;
	for (long int step = 0; step < maxStep; step++) {
		t = step*dt;
		SolveLaplaceEq_EP(U, T, M, tolerance);
		CalculateHeatFlow(U, T, M, H);
		#pragma omp parallel for collapse(3) schedule(dynamic)
		for (int k=1; k<kk-1; k++) {
			for (int j=1; j<jj-1; j++) {
				for (int i=1; i<ii-1; i++) {
					T(i,j,k) = T(i,j,k) + dt*H(i,j,k)/specificHeat(M(i,j,k));
				}
			}
		}
		if (step % outputPeriod == 0) {
			cout << "t= " << t << " Tmax= " << T.getNorm() << " Hmax= " << H.getNorm() << endl;
		}
		if (step % savePeriod == 0) {
			ostringstream vtkfilename;
			vtkfilename.str("");
			vtkfilename.clear();
			vtkfilename << "data/T-" << std::setfill('0') << std::setw(8) << step <<".vtk";
			T.saveVtk(vtkfilename);
		}
	}
}

#endif
