#include <iostream>
#include <stdlib.h>
#include "Solver.h"

int main(int argc, char ** argv) {
	//Mesh
	Mesh m(x_min,x_max,y_min,y_max,z_min,z_max,ii,jj,kk);
	// Scalar fields
	ScalarField M(m); // Material distribution in cell
	ScalarField U(m); // Electrical potential
	ScalarField T(m); // Temperature

	// Simulation
	BuildCell(M, true); // Set nodes' material
	T.init(T0); // Set initial temperature of nodes

	ostringstream vtkfilename;
	vtkfilename.str("");
	vtkfilename.clear();
	vtkfilename << "MaterialDistr.vtk";
	M.saveVtk(vtkfilename);

	SetDirichletBoundaryConditions(U);
	SolveLaplaceEq_EP(U, T, M, strtold(argv[1], NULL));

	vtkfilename.str("");
	vtkfilename.clear();
	vtkfilename << "InitialU.vtk";
	U.saveVtk(vtkfilename);

	return 0;
}
