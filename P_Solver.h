#ifndef P_SOLVER
#define P_SOLVER

#include <iostream>
#include <iomanip>
#include <mpi.h>
#include "ScalarField.h"
#include "Params.h"
#include "Solver.h"

using namespace Params;
using namespace std;

void SolveCoupledProblemOnMaster(const ScalarField& U, const ScalarField& T, const ScalarField& M,
	long double dt, long int maxStep, long double tolerance, long int outputPeriod, long int savePeriod);
void SolveCoupledProblemOnWorkers(long double dt, long int maxStep);
void SplitAndJoinDomain(const ScalarField& U, const ScalarField& T, const ScalarField& M, const ScalarField& H);

void P_CalculateHeatFlow(const ScalarField& U, const ScalarField& T, const ScalarField& M, const ScalarField& H) {
	//long double a, b, c, d, e, f, ijk_tc;
	long double dx = T.getMesh().getDx();
	long double dy = T.getMesh().getDy();
	long double dz = T.getMesh().getDz();
	long double dxsqr = pow(dx, 2.0);
	long double dysqr = pow(dy, 2.0);
	long double dzsqr = pow(dz, 2.0);

	//long double lapT, Q, Uxsqr, Uysqr, Uzsqr;

	for (int k=1; k<T.getMesh().getKK()-1; k++) {
		for (int j=1; j<jj-1; j++) {
			for (int i=1; i<ii-1; i++) {
				// Heat conduction
				long double ijk_tc = thermalConductivity(M(i,j,k));
				long double a = (thermalConductivity(M(i+1,j,k)) + ijk_tc)/(2*dxsqr);
				long double b = (thermalConductivity(M(i-1,j,k)) + ijk_tc)/(2*dxsqr);
				long double c = (thermalConductivity(M(i,j+1,k)) + ijk_tc)/(2*dysqr);
				long double d = (thermalConductivity(M(i,j-1,k)) + ijk_tc)/(2*dysqr);
				long double e = (thermalConductivity(M(i,j,k+1)) + ijk_tc)/(2*dzsqr);
				long double f = (thermalConductivity(M(i,j,k-1)) + ijk_tc)/(2*dzsqr);

				long double lapT = a*(T(i+1,j,k)-T(i,j,k)) + b*(T(i-1,j,k)-T(i,j,k)) + c*(T(i,j+1,k)-T(i,j,k))
				+ d*(T(i,j-1,k)-T(i,j,k)) + e*(T(i,j,k+1)-T(i,j,k)) + f*(T(i,j,k-1)-T(i,j,k));

				//Joule heating
				long double Uxsqr = pow((U(i+1,j,k) - U(i-1,j,k))/(2*dx), 2.0);
				long double Uysqr = pow((U(i,j+1,k) - U(i,j-1,k))/(2*dy), 2.0);
				long double Uzsqr = pow((U(i,j,k+1) - U(i,j,k-1))/(2*dz), 2.0);
				long double Q = electricalConductivity(M(i,j,k),T(i,j,k))*(Uxsqr + Uysqr + Uzsqr);
				H(i,j,k) = lapT + Q;
			}
		}
	}
}

void SolveCoupledProblemOnMaster(const ScalarField& U, const ScalarField& T, const ScalarField& M, long double dt,
	long int maxStep, long double tolerance, long int outputPeriod, long int savePeriod) {
	ScalarField H(T.getMesh());

	long double t;
	for (long int step = 0; step < maxStep; step++) {
		t = step*dt;
		SolveLaplaceEq_EP(U, T, M, tolerance);
		// Should use MPI here to slice domain for U, T, M and the call for the subdomains
		SplitAndJoinDomain(U, T, M, H);
		//CalculateHeatFlow(U, T, M, H);
		//
		for (int k=1; k<kk-1; k++) {
			for (int j=1; j<jj-1; j++) {
				for (int i=1; i<ii-1; i++) {
					T(i,j,k) = T(i,j,k) + dt*H(i,j,k)/specificHeat(M(i,j,k));
				}
			}
		}
		if (step % outputPeriod == 0) {
			cout << "t= " << t << " Tmax= " << T.getNorm() << " Hmax= " << H.getNorm() << endl;
		}
		if (step % savePeriod == 0) {
			ostringstream vtkfilename;
			vtkfilename.str("");
			vtkfilename.clear();
			vtkfilename << "data/PT-" << std::setfill('0') << std::setw(8) << step <<".vtk";
			T.saveVtk(vtkfilename);
		}
	}
}

void SolveCoupledProblemOnWorkers(long double dt, long double dz, long int maxStep) {
	MPI_Status status;
	int tag;
	int offset, size;
	long double *Umat, *Tmat, *Mmat;

	for (long int step = 0; step < maxStep; step++) {
		tag = 0; // U Scalar field
		MPI_Recv(&size, 1, MPI_INT, 0, tag, MPI_COMM_WORLD,  &status);
		Umat = new long double[size];
		MPI_Recv(Umat, size, MPI_LONG_DOUBLE, 0, tag, MPI_COMM_WORLD,  &status);

		tag = 1; // T Scalar field
		Tmat = new long double[size];
		MPI_Recv(Tmat, size, MPI_LONG_DOUBLE, 0, tag, MPI_COMM_WORLD,  &status);

		tag = 2; // M Scalar field
		Mmat = new long double[size];
		MPI_Recv(Mmat, size, MPI_LONG_DOUBLE, 0, tag, MPI_COMM_WORLD,  &status);

		int width = size / ii;
		width /= jj;
		Mesh m(x_min,x_max,y_min,y_max,0,dz*(width-1),ii,jj,width);
		ScalarField U(m), T(m), M(m);
		for (int k=0; k <= width - 1; k++) {
			for (int j=0; j <= jj-1; j++) {
				for (int i=0; i <= ii-1; i++) {
					U(i,j,k) = Umat[i+j*ii+ii*jj*k];
					T(i,j,k) = Tmat[i+j*ii+ii*jj*k];
					M(i,j,k) = Mmat[i+j*ii+ii*jj*k];
				}
			}
		}

		ScalarField H(T.getMesh());
		P_CalculateHeatFlow(U, T, M, H);

		tag = 3; // H Scalar field
		MPI_Send(&size, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
		MPI_Send(H.getMatrix(), size, MPI_LONG_DOUBLE, 0, tag, MPI_COMM_WORLD);

		/*int my_rank;
		MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
		ostringstream vtkfilename;
		vtkfilename.str("");
		vtkfilename.clear();
		vtkfilename << "data/U-" << my_rank << "-" << std::setfill('0') << std::setw(8) << step <<".vtk";
		U.saveVtk(vtkfilename);
		vtkfilename.str("");
		vtkfilename.clear();
		vtkfilename << "data/T-" << my_rank << "-" << std::setfill('0') << std::setw(8) << step <<".vtk";
		T.saveVtk(vtkfilename);
		vtkfilename.str("");
		vtkfilename.clear();
		vtkfilename << "data/M-" << my_rank << "-" << std::setfill('0') << std::setw(8) << step <<".vtk";
		M.saveVtk(vtkfilename);
		vtkfilename.str("");
		vtkfilename.clear();
		vtkfilename << "data/H-" << my_rank << "-" << std::setfill('0') << std::setw(8) << step <<".vtk";
		H.saveVtk(vtkfilename);*/
		delete[] Umat;
		delete[] Tmat;
		delete[] Mmat;
	}
}

void SplitAndJoinDomain(const ScalarField& U, const ScalarField& T, const ScalarField& M, const ScalarField& H) {
	//int my_rank; // rank of process
	int p; // number of processes
	int source; // rank of sender
	//int dest; // rank of receiver
	int tag; // tag for messages
	MPI_Status status; // return status for  receive

	MPI_Comm_size(MPI_COMM_WORLD, &p); // number of processes

	int sliceWidth = U.getMesh().getKK() / (p - 1);
	int lastSliceWidth =  U.getMesh().getKK() % (p - 1);
	int offset, size;

	long double *Hmat;

	for (int w = 1; w < p; w++) {
			offset = ii * jj * (w-1) * sliceWidth;
			if (w == 1) {
				size = (sliceWidth + 1)*ii*jj;
			} else {
				if ((w == p-1) && (lastSliceWidth == 0)) {
					size = (sliceWidth + 1)*ii*jj;
				} else {
					size = (sliceWidth + 2)*ii*jj;
				}
				offset -= (ii * jj);
			}
			tag = 0; // U Scalar field
			MPI_Send(&size, 1, MPI_INT, w, tag, MPI_COMM_WORLD);
			MPI_Send(U.getMatrix()+offset, size, MPI_LONG_DOUBLE, w, tag, MPI_COMM_WORLD);
			tag = 1; // T Scalar field
			//MPI_Send(&size, 1, MPI_INT, w, tag, MPI_COMM_WORLD);
			MPI_Send(T.getMatrix()+offset, size, MPI_LONG_DOUBLE, w, tag, MPI_COMM_WORLD);
			tag = 2; // M Scalar field
			//MPI_Send(&size, 1, MPI_INT, w, tag, MPI_COMM_WORLD);
			MPI_Send(M.getMatrix()+offset, size, MPI_LONG_DOUBLE, w, tag, MPI_COMM_WORLD);
	}

	for (int w = 1; w < p; w++) {
		//offset = ii * jj * (w-1) * sliceWidth;

		tag = 3; // H Scalar field
		MPI_Recv(&size, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD,  &status);
		source = status.MPI_SOURCE;
		//cout << "source: " << source << endl;
		Hmat = new long double[size];
		MPI_Recv(Hmat, size, MPI_LONG_DOUBLE, source, tag, MPI_COMM_WORLD,  &status);

		/*if (w == 1) {
			size = (sliceWidth + 1)*ii*jj;
		} else {
			if (w == p-1) {
				size = (sliceWidth + 1)*ii*jj;
			} else {
				size = (sliceWidth + 2)*ii*jj;
			}
		}*/

		int workerOffset = (source-1) * sliceWidth;
		int skipLeft = 0;
		if (source != 1) {
			skipLeft = 1;
		}

		for (int k = 0; k <= sliceWidth - 1; k++) {
			for (int j=0; j <= jj-1; j++) {
				for (int i=0; i <= ii-1; i++) {
					H(i,j,k + workerOffset) = Hmat[i+j*ii+ii*jj*(k+skipLeft)];
				}
			}
		}

		delete[] Hmat;
	}

	if (lastSliceWidth != 0) {
		//Master process solves subdomain
		int width = lastSliceWidth + 1;
		offset = (p-1) * sliceWidth - 1;
		Mesh m(x_min,x_max,y_min,y_max,0,U.getMesh().getDz()*(width-1),ii,jj,width);
		ScalarField subH(T.getMesh());
		ScalarField subU(m), subT(m), subM(m);
		for (int k=0; k <= width - 1; k++) {
			for (int j=0; j <= jj-1; j++) {
				for (int i=0; i <= ii-1; i++) {
					subU(i,j,k) = U(i,j,k+offset);
					subT(i,j,k) = T(i,j,k+offset);
					subM(i,j,k) = M(i,j,k+offset);
				}
			}
		}
		ostringstream vtkfilename;
		vtkfilename.str("");
		vtkfilename.clear();
		vtkfilename << "data/subU.vtk";
		subU.saveVtk(vtkfilename);

		P_CalculateHeatFlow(subU, subT, subM, subH);
		width = lastSliceWidth;
		offset = (p-1) * sliceWidth;
		for (int k=0; k <= width - 1; k++) {
			for (int j=0; j <= jj-1; j++) {
				for (int i=0; i <= ii-1; i++) {
					H(i,j,k+offset) = subH(i,j,k);
				}
			}
		}
	}

}

#endif
